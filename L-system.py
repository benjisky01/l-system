from turtle import *
import sys

def apply_rules(letter, rule_local):
    """Appliquez des règles à une lettre individuelle et renvoyez le résultat."""
    # Règle 1
    for k,v in rule_local.items():
        if letter == k:
            new_string = v
            break
        else:
            new_string = letter

    return new_string


def process_string(original_string, rule_local):
    """Appliquez des règles à une chaîne, une lettre à la fois, et renvoyez le résultat."""
    tranformed_string = ""
    for letter in original_string:
        tranformed_string = tranformed_string + apply_rules(letter, rule_local)

    return tranformed_string


def create_l_system(number_of_iterations, axiom, rule_local):
    """Commencez par un axiome et appliquez des règles au nombre d'itérations de la chaîne d'axiome d'origine, puis renvoyez le résultat."""
    start_string = axiom
    for counter in range(number_of_iterations):
        end_string = process_string(start_string, rule_local)
        start_string = end_string

    return end_string


def draw_l_system(instructions,angle,taille):
    """Dessinez avec une turtle, en interprétant chaque lettre dans les instructions transmises."""
    stack = []
    for task in instructions:
        if task == "a":
            pd()
            forward(taille)
            #print("pd();fd(",taille,")")
        elif task == "b":
            pu()
            forward(taille)
           # print("pu();fd(",taille,")")
        elif task == "+":
            right(angle)
           # print("right(",angle,")")
        elif task == "-":
            left(angle)
           # print("left(",angle,")")
        elif task == "[":
            stack.append((position(),heading()))
          #  print("mémoriser la position actuelle")
        elif task == "]":
            pu()
            pos, hea = stack.pop()
            goto(pos)
            setheading(hea)
        #    print("retourner à a dernière position mémorisée")
        elif task == "*":
            right(180)
           # print("right(180)")

def lignes(fichier):
    list_1=[]
    for line_in_file in fichier:
        q=line_in_file.replace("\n","")
        list_1.append(q)
    axi = 0;niv = 0;ang = 0;tai = 0;reg = 0
    for i in list_1:
        if "axiome" in i:
            axi = 1
        if "niveau" in i:
            niv = 1
        if "angle" in i:
            ang = 1
        if "taille" in i:
            tai = 1
        if "regles" in i:
            reg = 1
    if axi == 0:
        print("il n'y a pas axiome");sys.exit()
    elif niv == 0:
        print("il n'y a pas niveau");sys.exit()
    elif ang == 0:
        print("il n'y a pas angle");sys.exit()
    elif tai == 0:
        print("il n'y a pas taille");sys.exit()
    elif reg == 0:
        print("il n'y a pas regles");sys.exit()
    fichier.close()
    return list_1

def phan_tu(a):
    rule_1 = {}
    for i in a:
        try:
            if "axiome" in i:
                print(i)
                p_1 = i.index('"')
                #p_2 = i.rindex('"')
                axiome = i[p_1+1:i.index('"',p_1+1)]
            elif "niveau" in i:
                b_1=i.split("=")
                niveau = int(b_1[1].strip())
            elif "angle" in i:
                b_2=i.split("=")
                angle = float(b_2[1].strip())
            elif "taille" in i:
                b_3=i.split("=")
                taille = int(b_3[1].strip())
            elif "regles" in i:
                p_1 = i.index('"')
                p_2 = i.index('=')
                n_1=i[p_1+1:i.index('=',p_2+1)].strip()
                m_1=i[i.index('=',p_2+1)+1:i.index('"',p_1+1)].strip()
                rule_1[n_1]=m_1
            else:
                p_1 = i.index('"')
                p_2 = i.index('=')
                n_2 = i[p_1+1:p_2].strip()
                m_2 = i[p_2+1:i.index('"',p_1+1)].strip()
                rule_1[n_2]=m_2
        except:
            print("erreur de syntaxe")
            sys.exit()
    return axiome, niveau, angle, taille, rule_1
    
def setting(ang):
    screensize(1500,1500)
    #speed(0)
    tracer(5)
    setheading(ang)
    pensize(0)
    #exitonclick()

            
############################################################################
def main():
    nom_fichier = input("entrez un fichier : ")
    try:
        f=open(nom_fichier,"r")
    except:
        print("Entrez le mauvais nom")
        sys.exit()

    try:
        col= input("color : ")
        color(col)
    except:
        print("Entrez le mauvais color")
        sys.exit()
    
    print()
    print("from turtle import *")
    print("color('",col,"')")
    
    list_1=lignes(f)
    rule_1 = {}
    axiome, niveau, angle, taille, rule_1 = phan_tu(list_1)
    
    # create the string of turtle instructions
    instruction_string = create_l_system(niveau, axiome, rule_1)

    print(instruction_string)

    setting(angle)

    draw_l_system(instruction_string,angle,taille)

    print("exitonclick")
if __name__ == "__main__":
    main()
